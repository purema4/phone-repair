window.onload = function () {

	//adding click with the link for each buttons, je pourrais faire autrement lol
	const buttons = document.querySelectorAll("button")
	for (button of buttons) {
		button.onclick = e => {
			window.location = e.target.dataset.link
		}
	}

	const ulLiens = document.querySelector(".liens")
	const ham = document.querySelector(".hamburger").addEventListener("click", e => {

		ulLiens.classList = "liens active"
		ulLiens.style.transform = "translateY(0)";

		//add listener on black bg when active.

		ulLiens.addEventListener("click", e => {
			ulLiens.classList = "liens"
			ulLiens.style.transform = "translateY(100%)";
		})

	})

	//carroussel

	const leftA = document.querySelector("#left-arrow")
	const rightA = document.querySelector("#right-arrow")

	let carrValue = 0

	leftA.addEventListener("click", e => {
		carrValue -= 100;
		checkCarrVal()
		updateCarr()
		updatedots()
	})

	rightA.addEventListener("click", e => {
		carrValue += 100;
		checkCarrVal()
		updateCarr()
		updatedots()
	})

	function checkCarrVal() {

		if (carrValue < 0) {
			carrValue = 200
		} else if (carrValue > 200) {
			carrValue = 0
		}

	}


	const dotsCarr = document.querySelectorAll(".dot");

	function updatedots() {

		let tempCarrValue = carrValue / 100

		for (dot of dotsCarr) {
			dot.classList = "dot"
		}

		dotsCarr[tempCarrValue].classList = "dot active"

	}

	const picCarr = document.querySelector(".pictures")

	function updateCarr() {
		picCarr.style.transform = `translateX(-${carrValue}vw)`;
	}

	/*setInterval(function () {
		carrValue += 100;
		checkCarrVal()
		updateCarr()
		updatedots()
	}, 3000)
*/
}